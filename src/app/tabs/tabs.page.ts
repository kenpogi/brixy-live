import { Component, OnInit, NgZone  } from '@angular/core';
import { Platform,  AlertController, NavController} from '@ionic/angular';
import { Router } from '@angular/router';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { Vibration } from '@ionic-native/vibration/ngx';


@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  subscription: any;

  constructor(
    public platform: Platform, 
    public alertCtrl: AlertController, 
    private router: Router,
    private appMinimize: AppMinimize,
    private localNotifications: LocalNotifications,
    private postPvdr: PostProvider,
    private storage: Storage,
    private zone: NgZone,
    public navCtrl: NavController,
    private vibration: Vibration
    ) { 
    // this.platform.backButton.subscribe(() => {
    //   console.log("you got it all SM");
    // });
  }

  toAccount(){
    this.router.navigate(['tabs/myaccount']);
  }
  toTab3(){
    this.router.navigate(['tabs/tab3']);
  }

  ngOnInit() {
    this.plotData();
  }

  ionViewDidEnter(){
    this.subscription = this.platform.backButton.subscribe(()=>{
       // navigator['app'].exitApp();
       this.router.navigate(['tabs']);
       console.log("hello darkness my old friend");

       this.alertCtrl.create({
        header: 'Brixy Live',
        message: 'Do you want to close the app?',
        buttons: [{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
                console.log('Application exit prevented!');
            }
        },{
            text: 'Close App',
            handler: () => {
              //navigator['app'].exitApp(); // Close this application
              this.appMinimize.minimize();
            }
        }]
      }).then(res => {
        res.present();
      });

    });

    


    

    
  }

  plotData(){
    setInterval(()=>{                           //<<<---using ()=> syntax
        this.zone.run(()=>{

        //console.log("magbalik ka na naman");
        this.storage.get('brixy_user_id').then((user_id) => {
        
            let body2 = {
              action : 'getNewMessage',
              type : 1,
              user_id : user_id
            };
            //console.log("hain ka:"+JSON.stringify(body2));
            this.postPvdr.postData(body2, 'messages.php').subscribe(data => {
              console.log(data);
              if(data.success){

                for(const key in data.result){


                  // console.log("data.result.user_id:"+data.result[key].user_sent);
                  // console.log("data.result.myNickname:"+data.result[key].myNickname);
                  // console.log("data.result.message:"+data.result[key].message);
                  // console.log("user_id:"+user_id);

                  if(data.result[key].user_sent != user_id){

                    this.localNotifications.schedule({
                      id: data.result[key].id,
                      title: "New Message",
                      text: data.result[key].myNickname+": "+data.result[key].message,
                      trigger: {
                        in: 1,
                        unit: ELocalNotificationTriggerUnit.SECOND,
                      },
                    });

                    this.vibration.vibrate(1000);
                    this.localNotifications.on('click').subscribe(()=>{


                      //console.log("you have clicked this man")
                      this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id]);


                    });

                  }
                }
              
              }
            });


            // for system notifications

          // console.log("system notif");
            let body3 = {
              action : 'getSystemNotifications',
              user_id : user_id
            };
            this.postPvdr.postData(body3, 'system_notification.php').subscribe(data => {
              
              if(data.success){


                for(const key in data.result){


                    this.localNotifications.schedule({
                      id: data.result[key].id,
                      title: "Brixy Live Notification",
                      text: data.result[key].notification_message,
                      trigger: {
                        in: 1,
                        unit: ELocalNotificationTriggerUnit.SECOND,
                      },
                    });

                    this.vibration.vibrate(1000);
                    // This is for redirecting when the user click the notification
                    //this.localNotifications.on('click').subscribe(()=>{

                    //   this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id]);


                    // });

                  
                }


              }

            });

            // for announcement

            // console.log("getannouncement")
            let body4 = {
              action : 'getAnnouncement',
              user_id : user_id
            };
            this.postPvdr.postData(body4, 'system_notification.php').subscribe(data => {
              
              if(data.success){


                for(const key in data.result){


                    this.localNotifications.schedule({
                      id: data.result[key].id,
                      title: data.result[key].title,
                      text: data.result[key].message,
                      trigger: {
                        in: 1,
                        unit: ELocalNotificationTriggerUnit.SECOND,
                      },
                    });

                    this.vibration.vibrate(1000);
                    // This is for redirecting when the user click the notification
                    // this.localNotifications.on('click').subscribe(()=>{

                    //   this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id]);


                    //  });

                  
                }


              }

            });

      
          }); // end of storage

      });// end of zone
        

    }, 5000);
  }

  ionViewWillLeave(){
      this.subscription.unsubscribe();
  }

}
