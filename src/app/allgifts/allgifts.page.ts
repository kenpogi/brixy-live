import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { SendgiftbronzemodalPage } from '../sendgiftbronzemodal/sendgiftbronzemodal.page';
import { SendgiftsilvermodalPage } from '../sendgiftsilvermodal/sendgiftsilvermodal.page';
import {Howl, Howler} from 'howler';
import { ModalController, NavParams } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { giftModel } from '../shared/model/gifts.model';
import { PostProvider } from 'src/providers/credential-provider';



@Component({
  selector: 'app-allgifts',
  templateUrl: './allgifts.page.html',
  styleUrls: ['./allgifts.page.scss'],
})
export class AllgiftsPage implements OnInit {

  player: Howl = null;
  live_user_id : string;

  user_experience: number = 0;
  user_coins : number = 0;

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;

  constructor(
    private modalController: ModalController,
    private router: Router,
    private storage: Storage,
    navParams: NavParams,
    private postPvdr: PostProvider
    ) { 

      this.live_user_id = navParams.get('live_user_id');

    }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

   async sendgift() {
        const modal = await this.modalController.create({
          component: SendgiftbronzemodalPage,
          cssClass: 'sendgiftmodal'
        });
     
        return await modal.present();
      }
  viewlive(){
    this.router.navigate(['viewlive']);
  }

  bronzeList: giftModel[] = [];
    
    //  {
    //    //not yet
    //   name:'Brixy Butterfly Basic Bronze',
    //  image:'assets/gif/rainbow.gif',
    //  imagegif: 'assets/gif/rainbow.gif',
    //  imageaudio: 'assets/gifaudio/lips.mp3',
    //  imageaudio2: '',
    //  price:"5000"
    //  }


    //  async openBronze(bronze) {

    //       const modal = await this.modalController.create({
    //         component: SendgiftbronzemodalPage,
    //         cssClass: 'sendgiftmodal',
    //         componentProps: { bronze: bronze, 'live_user_id' : this.live_user_id}
    //       });
  
    //   if (this.player != null) {
    //     this.player.stop();
    //     this.player.unload();
    //     this.player = null;
    // }
  
    //       this.player = new Howl({
    //         src: [bronze.imageaudio],
    //         onend: function() {
    //             this.player = new Howl({
    //                   src: [bronze.imageaudio2]
    //                 });
    //                 this.player.play();
    //               }
    //       });
        
    //       this.player.play();
    //       Howler.volume(0.5);
    //    
    //       return await modal.present();
     
    //       }

    silverList: giftModel[] = [];
       
    
       
    // silver page is the one used for all
    async openSilver(silver) {


        const modal = await this.modalController.create({
          component: SendgiftsilvermodalPage,
        cssClass: 'sendgiftmodal',
        componentProps: { silver: silver, 'live_user_id' : this.live_user_id}
        });

    // if (this.player != null) {
    // this.player.stop();
    // this.player.unload();
    // this.player = null;
    // }

    //   this.player = new Howl({
    //     src: [silver.imageaudio]
    //   });

    //   this.player.play();
    //   Howler.volume(0.5);
     
        return await modal.present();
  }


    
      goldList: giftModel[] = [];
      
    
      popularList: giftModel[] = [];

  ngOnInit() {
    this.plotData();
  }

  async plotData(){
    // for bronze
    let body = {
      action : 'showGifts',
      type : 1
    };
    this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
      console.log(data);
      if(data.success){

        for(const key in data.result){
          this.bronzeList.push(new giftModel(
            data.result[key].id,
            data.result[key].gifts,
            data.result[key].image_gif,
            data.result[key].image_gif,
            data.result[key].audio,
            '',
            data.result[key].gold_bar
            
            ));
          //console.log(data.result[key].user_id);
        }

         
      }
    });

    this.storage.get("brixy_user_id").then((user_id) => {

      let body = {
        action : 'getGold_bar',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
        
        if(data.success){
          for(const key in data.result){
            this.user_experience = data.result[key].user_experience;
            this.user_coins = data.result[key].user_coins;
            
          }
        }
      });
    });


    // for silver
    let body2 = {
      action : 'showGifts',
      type : 2
    };
    this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
     ;
      if(data.success){

        for(const key in data.result){
          this.silverList.push(new giftModel(
            data.result[key].id,
            data.result[key].gifts,
            data.result[key].image_gif,
            data.result[key].image_gif,
            data.result[key].audio,
            '',
            data.result[key].gold_bar
            
            ));
        }
         
      }
    });


    // for gold

    let body3 = {
      action : 'showGifts',
      type : 3
    };
    this.postPvdr.postData(body3, 'brixy-store.php').subscribe(data => {
    
      if(data.success){

        for(const key in data.result){
          this.goldList.push(new giftModel(
            data.result[key].id,
            data.result[key].gifts,
            data.result[key].image_gif,
            data.result[key].image_gif,
            data.result[key].audio,
            '',
            data.result[key].gold_bar
            
            ));
        }
         
      }
    });

    //for popular gift

    let body4 = {
      action : 'showGifts',
      type : 4
    };
    this.postPvdr.postData(body4, 'brixy-store.php').subscribe(data => {
    
      if(data.success){

        for(const key in data.result){
          this.popularList.push(new giftModel(
            data.result[key].id,
            data.result[key].gifts,
            data.result[key].image_gif,
            data.result[key].image_gif,
            data.result[key].audio,
            '',
            data.result[key].gold_bar
            
            ));
        }
         
      }
    });

  }// end of plot data


  closemodal()
  {
    this.modalController.dismiss();
  }

}
