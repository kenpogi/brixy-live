import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { paychartModel } from '../shared/model/paychart.model';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-payreqconfirmation',
  templateUrl: './payreqconfirmation.page.html',
  styleUrls: ['./payreqconfirmation.page.scss'],
})
export class PayreqconfirmationPage implements OnInit {

  paychart: paychartModel;
  cashout_status: number = 0;
  requested_gold_bar: number = 0;

  gold_bar : number;

  peak_gold_bar : number;

  in_gold_bar: number = 0;
  in_usd_amount: number = 0;

  myGoldBars: number = 0;
     myUsdAmount: number = 0;

  custom: boolean = false;

  constructor(
    navParams: NavParams, 
    private storage: Storage, 
    public modalCtrl: ModalController,
    private toastController: ToastController,
    private postPvdr: PostProvider
  ) {

    this.paychart = navParams.get('paychart');

    

   }

  ngOnInit() {
    this.plotData();
    
  }
  ionViewWillEnter(){
    if(this.paychart.id == 0){
      this.custom = true;
      console.log("custom:"+this.custom);
    }
  }

  plotData(){
    this.storage.get("brixy_user_id").then((user_id) => {

      let body34 = {
        action : 'checkCashoutStatus',
        user_id : user_id
      };
      this.postPvdr.postData(body34, 'payment.php').subscribe(async data => {
        if(data.success){

          this.peak_gold_bar = parseInt(data.peak_gold_bar);
          this.cashout_status = data.cashout_status;

          if(data.requested_gold_bar!=null){
            this.requested_gold_bar = parseInt(data.requested_gold_bar);
          }

          
        }
      });


      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){

          this.gold_bar = parseInt(data.result.gold_bar);
        }
      });



    });
  }

  closeModal()
    {
      this.modalCtrl.dismiss();
    }

  confirmPaymentRequest(){

    console.log((this.peak_gold_bar+2));
    console.log((this.requested_gold_bar+3));
    console.log((this.gold_bar+4));

    

      if(this.custom){
        this.myGoldBars = this.in_gold_bar;
        this.myUsdAmount = this.in_usd_amount;
      }
      else{
        this.myGoldBars = this.paychart.gold_bars;
        this.myUsdAmount = this.paychart.usd_amount;
      }
      var total = (((this.myGoldBars+this.peak_gold_bar)+(this.requested_gold_bar)));
      console.log("goldbarsmy:"+((this.myGoldBars)));
      console.log("pila man jud ang total:"+total);
    if((this.gold_bar)<total){
      this.presentToast('You have insufficient balance of gold bars. ');
    }
    else{

      

      this.storage.get("brixy_user_id").then((user_id) => {

        let body2 = {
          action : 'requestPayment',
          user_id : user_id,
          gold_bars : this.myGoldBars,
          usd_amount : this.myUsdAmount,
          
        };
        console.log("body:"+JSON.stringify(body2));
        this.postPvdr.postData(body2, 'payment.php').subscribe(data => {
          
          if(data.success){
            this.presentToast('Cashout Requested Successfully.');
            this.modalCtrl.dismiss();
          }
        });
  
      });


    }


    
  }

  async presentToast(toastMessage) {
    const toast = await this.toastController.create({
      message: toastMessage,
      duration: 3000
    });
    toast.present();
  }

}
