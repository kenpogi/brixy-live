import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VipackagePageRoutingModule } from './vipackage-routing.module';

import { VipackagePage } from './vipackage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VipackagePageRoutingModule
  ],
  declarations: [VipackagePage]
})
export class VipackagePageModule {}
