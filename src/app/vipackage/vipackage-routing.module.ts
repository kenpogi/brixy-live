import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VipackagePage } from './vipackage.page';

const routes: Routes = [
  {
    path: '',
    component: VipackagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VipackagePageRoutingModule {}
