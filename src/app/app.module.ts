import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ToastController } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { GiftbronzeComponent } from './giftbronze/giftbronze.component';
import { GiftgoldComponent } from './giftgold/giftgold.component';
import { GiftsilverComponent } from './giftsilver/giftsilver.component';
import { GiftpopularComponent } from './giftpopular/giftpopular.component';

import { Facebook } from '@ionic-native/facebook/ngx';

import { SendgiftbronzemodalPageModule } from './sendgiftbronzemodal/sendgiftbronzemodal.module';
import { SendgiftsilvermodalPageModule } from './sendgiftsilvermodal/sendgiftsilvermodal.module';
import { SendgiftgoldmodalPageModule } from './sendgiftgoldmodal/sendgiftgoldmodal.module';
import { SendgiftpopularmodalPageModule } from './sendgiftpopularmodal/sendgiftpopularmodal.module';
import { HistorypreviewPageModule } from './historypreview/historypreview.module';
import { HistorygiftpreviewPageModule } from './historygiftpreview/historygiftpreview.module';
import { AllgiftsPageModule } from './allgifts/allgifts.module';
import { LiveprofilePageModule } from './liveprofile/liveprofile.module';
import { ReportuserPageModule } from './reportuser/reportuser.module';
import { VippackagePageModule } from './vippackage/vippackage.module';
import { RegisterPageModule } from './register/register.module';
import { PurchasecoinsPageModule } from './purchasecoins/purchasecoins.module';

import { PayreqconfirmationPageModule } from './payreqconfirmation/payreqconfirmation.module';

import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';

import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { PostProvider } from '../providers/credential-provider';
import { ChatMessages } from '../providers/chat-messages.provider';
import { SmsProvider } from '../providers/sms-provider';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { PayPal } from '@ionic-native/paypal/ngx';

import { AppMinimize } from '@ionic-native/app-minimize/ngx';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://128.199.154.230:3001', options: {} };

//const config: SocketIoConfig = { url: 'http://192.168.1.5:3001', options: {} };

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInSuccessUrl: '/tabs',
  siteName: 'Brixy Live',
  signInFlow: 'popup',
  signInOptions: [
    firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  ],
  tosUrl: '/termsconditions',
  privacyPolicyUrl: '/termsconditions',
  credentialHelper: firebaseui.auth.CredentialHelper.NONE
};


@NgModule({
  declarations: [AppComponent,GiftbronzeComponent,GiftgoldComponent,
    GiftsilverComponent,GiftpopularComponent],
  entryComponents: [GiftbronzeComponent,GiftgoldComponent,
    GiftsilverComponent,GiftpopularComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SendgiftbronzemodalPageModule,
    SendgiftsilvermodalPageModule,
    SendgiftgoldmodalPageModule,
    SendgiftpopularmodalPageModule,
    PurchasecoinsPageModule,
    PayreqconfirmationPageModule,
    HistorypreviewPageModule,
    HistorygiftpreviewPageModule,
    AllgiftsPageModule,
    RegisterPageModule,
    VippackagePageModule,
    LiveprofilePageModule,
    ReportuserPageModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    SocketIoModule.forRoot(config)
  ],
  providers: [
    AndroidPermissions,
    Camera,
    StatusBar,
    SplashScreen,
    FileTransfer,
    File,
    Facebook,
    GooglePlus,
    PostProvider,
    ChatMessages,
    SmsProvider,
    ToastController,
    VideoPlayer,
    AppMinimize,
    LocalNotifications,
    Vibration,
    PayPal,
    YoutubeVideoPlayer,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
