import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { paychartModel } from '../shared/model/paychart.model';
import { PostProvider } from '../../providers/credential-provider';
import { ModalController, ToastController } from '@ionic/angular';
import { PayreqconfirmationPage } from '../payreqconfirmation/payreqconfirmation.page';

@Component({
  selector: 'app-paymentrequest',
  templateUrl: './paymentrequest.page.html',
  styleUrls: ['./paymentrequest.page.scss'],
})
export class PaymentrequestPage implements OnInit {

  gold_bar : number;
  user_coins : number;
  cashout_status: number;

  peak_gold_bar : number;
  accumulated_settings: number;

  paychartList : paychartModel[] = [];

  payoneerId: string = "";

  overAccumulated: boolean = false;

  constructor(
    private storage: Storage,
    private postPvdr: PostProvider,
    public modalController: ModalController,
    private toastController: ToastController,
    ) { }

  goBack(){
    window.history.back();
  }

  ngOnInit() {
    this.plotData();
  }


  async paymentRequest(x){


    let paychart = this.paychartList.find(element=> element.id == x);

    if(x==123){
      paychart = new paychartModel(
        0,0,0
      );
    }
    console.log("usd_amount:"+paychart.usd_amount);

    // console.log("paychart:"+JSON.stringify(paychart));
    // console.log("this.peak_gold_gar:"+this.peak_gold_bar);
    // if((this.gold_bar)<(paychart.gold_bars+this.peak_gold_bar)){
    //   this.presentToast();
    // }
    // else{
      const modal = await this.modalController.create({
        component: PayreqconfirmationPage,
        cssClass: 'paymentrequest',
        componentProps: { paychart: paychart}
      });

        modal.onDidDismiss()
        .then((data) => {
          //const user = data['data']; // Here's your selected user!
          //this.showCoinsandGold();
        });

      return await modal.present();
   // }



  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'You have insufficient balance of gold bars. ',
      duration: 3000
    });
    toast.present();
  }

  plotData(){
    this.storage.get("brixy_user_id").then((user_id) => {

      let body34 = {
        action : 'checkCashoutStatus',
        user_id : user_id
      };
      this.postPvdr.postData(body34, 'payment.php').subscribe(async data => {
        if(data.success){

          this.peak_gold_bar = data.peak_gold_bar;
          this.accumulated_settings = parseInt(data.accumulated_settings);
          this.cashout_status = data.cashout_status;
          

        }
      });


      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){

          this.gold_bar = data.result.gold_bar;
          this.user_coins = data.result.user_coins;
        }
      });

      //for custom payout
      let body221 = {
        action : 'getAccumulated',
        user_id : user_id
      };
      this.postPvdr.postData(body221, 'payment.php').subscribe(data => {

        if(data.success){

          // console.log("data.accumulated:"+data.accumulated);
          // console.log("accumulated_setting:"+this.accumulated_settings);
          if(parseInt(data.accumulated) >= this.accumulated_settings){
            this.overAccumulated = true;
          }
        }
      });



      let body3 = {
        action : 'getPayoneerId',
        user_id : user_id
      };
      console.log("what?:"+JSON.stringify(body3));
      this.postPvdr.postData(body3, 'payment.php').subscribe(data => {

        if(data.success){

          for(const key in data.result){

              this.payoneerId = data.result[key].payment_id;

          }

          console.log("mypayoneerId:"+this.payoneerId);
        }
      });


    });

    let body2 = {
      action : 'showPaychart'
    };
    this.postPvdr.postData(body2, 'payment.php').subscribe(data => {

      if(data.success){
        const paychart: paychartModel[] = [];
        for(const key in data.result){

          paychart.push(new paychartModel(
            data.result[key].id,
            data.result[key].gold_bars,
            data.result[key].usd_amount
          ));
        }

        // const found = paychart.find(element=> element.id == 1);
        // console.log("found:"+JSON.stringify(found));
        this.paychartList = paychart;
      }
    });



  }

}
