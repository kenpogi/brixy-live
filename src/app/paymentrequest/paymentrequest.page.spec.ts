import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymentrequestPage } from './paymentrequest.page';

describe('PaymentrequestPage', () => {
  let component: PaymentrequestPage;
  let fixture: ComponentFixture<PaymentrequestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentrequestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymentrequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
