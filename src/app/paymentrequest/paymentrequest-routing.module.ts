import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentrequestPage } from './paymentrequest.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentrequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentrequestPageRoutingModule {}
