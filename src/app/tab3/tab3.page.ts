import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page implements OnInit {

  nickname : string;
  city : string;
  login_type_id: string;
  user_name: string;
  number : string;
  email : string;
  picture: string;
  login_icon : string;
  login_name : string;

  broadcast_topic: string = '';

  constructor(
    private router: Router,
    private storage: Storage,
    private postPvdr: PostProvider
    ) { }

  goTabs(){
    this.router.navigate(['tabs']);
    
  }

  goTerms(){
    console.log("bord:"+this.broadcast_topic);
    this.storage.set('broadcast_topic', this.broadcast_topic);
    this.router.navigate(['termsconditions']);
  }

  ngOnInit() {
    this.plotData();
  }


  plotData(){
    

    this.storage.get("brixy_user_id").then((user_id) => {
      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){



          this.picture = (data.result.photo == '') ? '' :
          this.postPvdr.myServer()+"/brixy-live/images/"+data.result.photo;
          this.nickname = data.result.nickname;
          this.city = data.result.city;
          this.login_type_id = data.result.login_type_id

          console.log("login_type_id:"+this.login_type_id);

          if(this.login_type_id == '1'){
            this.login_icon = "assets/icon/facebook.png";
            this.login_name = data.result.fname
          }
          else if(this.login_type_id == '2'){
            this.login_icon = "assets/icon/facebook.png";
            this.login_name = data.result.email
          }
          else if(this.login_type_id == '3'){
            this.login_icon = "assets/icon/sms.png";
            this.login_name = data.result.mobile_num
          }
          else if(this.login_type_id == '4'){
            this.login_icon = "assets/icon/mail.png";
            this.login_name = data.result.email
          }
        } 

      });
});

  }

}
