import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import {Howl, Howler} from 'howler';
import { PostProvider } from 'src/providers/credential-provider';
import { giftModel } from '../shared/model/gifts.model';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-sendgiftbronzemodal',
  templateUrl: './sendgiftbronzemodal.page.html',
  styleUrls: ['./sendgiftbronzemodal.page.scss'],
})
export class SendgiftbronzemodalPage implements OnInit {
  bronze: giftModel;
  player: Howl = null;
  live_user_id : string;

  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastController: ToastController,
    private postPvdr: PostProvider
    ) {
  
  this.bronze = navParams.get('bronze');
  this.live_user_id = navParams.get('live_user_id');
  
  }
  
    closeModal()
    {
      this.modalCtrl.dismiss();
      Howler.volume(0.0);
    }
  
    ngOnInit() {
      
    }
    ionViewWillLeave(){
      Howler.volume(0.0);
    }

    sendGift(){
      
      this.storage.get("brixy_user_id").then((user_id) => {

        let body = {
          action : 'checkBalance',
          user_id : user_id
          
        };
        //console.log("sendGifts:"+JSON.stringify(body2));
        this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
          
          if(data.success){

            if(data.result.gold_bar>=this.bronze.price){

              let body2 = {
                action : 'sendGifts',
                user_id : user_id,
                live_user_id : this.live_user_id,
                economy_id : this.bronze.id,
                amount : this.bronze.price
                
              };
              //console.log("sendGifts:"+JSON.stringify(body2));
              this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
                
                if(data.success){
                  this.modalCtrl.dismiss();
                  this.presentToast('Sent gift successfully.');
                   
                }
              });

            }
            else{
              this.presentToast('Insufficient balance of gold bars.');
            }
            
          }
          else{
            this.modalCtrl.dismiss();
            this.presentToast('Error.');
          }
        });
        
        
        

      });


    }

    async presentToast(toastMessage) {
      const toast = await this.toastController.create({
        message: toastMessage,
        duration: 3000
      });
      toast.present();
    }
  
  }