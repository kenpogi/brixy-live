import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController} from '@ionic/angular';

import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { ReportuserPage } from '../reportuser/reportuser.page';

@Component({
  selector: 'app-liveprofile',
  templateUrl: './liveprofile.page.html',
  styleUrls: ['./liveprofile.page.scss'],
})
export class LiveprofilePage implements OnInit {

  @Input() liveStreamProfileId: any;

  user_id: number;
  fname: string;
  lname: string;
  city: string;
  country: string;
  user_photo: string;
  user_level: number;
  user_locator_id: string;
  followers: number;
  following: number;
  gold_bars : number;
  user_experience : number;

  user_coins: number;
  bracket_to : number;

  badge_name : string;

  followClass = "myHeartFollow";

  status: boolean = false;

  constructor(
    private router: Router,
    public modalController: ModalController,
    private storage: Storage,
    private postPvdr: PostProvider,
    public navCtrl: NavController
    ) {}

  ngOnInit() {
    //console.log("boycott nba:"+this.liveStreamProfileId);
    this.plotData();
  }

  goBack(){
    this.modalController.dismiss();
  }

  message(){
    console.log("hello yellow");
    this.goBack();
    this.navCtrl.navigateForward(['/message/'+this.liveStreamProfileId]);
  }

  plotData(){

    let body = {
      action : 'getUser_liveData',
      user_id : this.liveStreamProfileId
    };
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
      
      if(data.success){

        
        for(const key in data.result){


            this.user_id = data.result[key].user_id;
            this.fname = data.result[key].fname;
            this.lname = data.result[key].lname;
            this.city = data.result[key].city;
            this.country = data.result[key].country;
            this.user_photo = (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            this.user_level = data.result[key].user_level;
            this.user_locator_id = data.result[key].user_locator_id;
            this.followers = data.result[key].followers;
            this.following = data.result[key].following;

            this.gold_bars = data.result[key].gold_bar;
            this.user_experience = data.result[key].user_experience;

            this.badge_name = data.result[key].badge_name;

            this.user_coins = data.result[key].user_coins;
            this.bracket_to = data.result[key].bracket_to;
         
        }
      }
    });

    this.storage.get('brixy_user_id').then((user_id) => {

      let body2 = {
        action : 'followerStatus',
        followed_by : user_id,
        user_id : this.liveStreamProfileId
  
      };
      this.postPvdr.postData(body2, 'followers.php').subscribe(async data => {
        
        if(data.success){
          if(data.result=="1"){
            this.status = true;
          }
        }
      });

    });
    
  }

  async reportUser() {
        const modal = await this.modalController.create({
          component: ReportuserPage,
          cssClass: 'liveprofilemodalstyle',
          componentProps: { 
            reportedId : this.liveStreamProfileId,
            reportedName: this.fname+' '+this.lname,
          }
        });
    
    
        modal.onDidDismiss()
        .then((data) => {
          //const user = data['data']; // Here's your selected user!
          console.log("dismiss of liveprofilepage modal")
          //this.plotFollowStatus();
        });
    
    
        return await modal.present();
      }

  follow(){



    this.storage.get('brixy_user_id').then((user_id) => {
     
      console.log("user_id follow:"+user_id);
        let body = {
          action : 'follow',
          user_id : this.user_id,
          followed_by : user_id,
          status : this.status
        };

        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
          console.log(data);
          if(data.success){
            
              this.followClass = "myHeartFollow";
              this.status = !this.status; 
              if(this.status){

                let body2 = {
                  action : 'addNotification',
                  user_id : this.user_id,
                  followed_by: user_id,
                  notification_message : "has followed you",
                  status : this.status
                };
        
                this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {

                })
              }
          }
        });
      });
   
  }

}
