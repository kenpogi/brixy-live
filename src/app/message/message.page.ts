import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { ToastController } from '@ionic/angular';
import { ChatMessages} from '../../providers/chat-messages.provider';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { chat, Conversations } from '../shared/model/chat.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit, OnDestroy {

  message = '';
  messages = [];
  currentUser = '';

  receiversId = '';
  session_id = '';
  user_id = '';

  nickname = '';

  messagesFromDB: chat[] = [];

  constructor(
    private router: Router,
    private chatMessages: ChatMessages,
    private storage: Storage,
    public activatedRoute: ActivatedRoute,
    private postPvdr: PostProvider,
    private socket: Socket, private toastCtrl: ToastController,
    public navCtrl: NavController) { }

  goTab5(){
    //this.router.navigate(['tabs/tab5']);
    window.history.back();
    //this.navCtrl.navigateRoot(['tabs/tab5']);
  }

  // sendMessage() {
  //   let body = {
  //     action : "addMessage"
  //   };
  //   this.chatMessages.sendMessage(body, 'credentials-api.php').subscribe(data => {
  //     if(data.success){

  //     }
  //   });


  // }

  ngOnInit(){} 

  ngOnDestroy(){
    this.socket.disconnect();
  }
  
  async ionViewWillEnter() {

    var paramId = this.activatedRoute.snapshot.paramMap.get("id");

    this.receiversId = paramId;
    console.log("hey");
    await this.plotData(this.receiversId);
    




    // let body = {
    //   action : "addMessage"
    // };
  //   this.chatMessages
  //     .getMessages(body, 'credentials-api.php')
  //     .distinctUntilChanged()
  //     .filter((message) => message.trim().length > 0)
  //     .throttleTime(1000)
  //     .scan((acc: string, message: string, index: number) =>
  //         `${message}(${index + 1})`
  //       , 1)
  //     .subscribe((message: string) => {
  //       // const currentTime = moment().format('hh:mm:ss a');
  //       // const messageWithTimestamp = `${currentTime}: ${message}`;
  //       // this.messages.push(messageWithTimestamp);
  //     });
  // }
  }

  sendMessage() {
    this.socket.emit('send-message', { 
      user_id: this.user_id, 
      room: this.session_id,
       text: this.message,
       vip_packageOfUser: "" });

 

    let body = {
      action : 'insertConvo',
      user_id : this.user_id,
      receiversId : this.receiversId,
      session_id : this.session_id,
      message : this.message
    };
    console.log("sessionId in inserting new message:"+this.session_id);
    this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
      if(data.success){
        

      }

    });



    this.message = '';

  }

  ionViewWillLeave() {

    console.log("will leave message and disconnect to socket");

    if(this.session_id != ''){

      let body = {
        action : 'updateConvoStatus',
        receiversId : this.receiversId,
        session_id : this.session_id
      };
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){
          
  
        }
  
      });

    }

    
    this.socket.disconnect();
  }

 async plotData(val){

    await this.storage.get("user_id").then( async (user_id) => {
      let body = {
        action : 'getSessionId',
        user_id : user_id,
        receiversId : val
      };
      //console.log("getConvo:"+JSON.stringify(body));
       this.postPvdr.postData(body, 'messages.php').subscribe( async data => {
        if( data.success){
          this.session_id =  data.session_id;
          this.user_id =  user_id;

          

          console.log("hey123");
          this.messagesFromDB = [];
      
          this.socket.connect();
      
      
          // Join chatroom
          console.log("user_id:"+this.user_id+"room:"+this.session_id);
          this.socket.emit('joinRoom', { 
            user_id: this.user_id, room: this.session_id, profile_picture: '', vip_packageOfUser: ""});
      
          //let name = `user-${new Date().getTime()}`;
          let name = this.nickname;
          this.currentUser = name;
      
          //this.socket.emit('set-name', name);
      
          // this.socket.fromEvent('users-changed').subscribe(data => {
          //   let user = data['user'];
          //   if (data['event'] === 'left') {
          //     this.showToast('User left: ' + user);
          //   } else {
          //     this.showToast('User joined: ' + user);
          //   }
          // });
      
          this.socket.fromEvent('message').subscribe(message => {
            this.messages.push(message);
            console.log("human og send"+ JSON.stringify(message));
          });
      
          this.socket.fromEvent('joinRoomMessage').subscribe(message => {
            console.log("message from server:"+message);
            
          });


          if(data.session_chat != '0'){
            let body3 = {
              action : 'getConvo',
              session_id : this.session_id

            };
            console.log("getConvo:"+JSON.stringify(body3));
            this.postPvdr.postData(body3, 'messages.php').subscribe(async data => {
              if(data.success){
                

                for(const key in data.result){
                  this.messagesFromDB.push(new chat(
                    data.result[key].id,
                    data.result[key].session_id,
                    data.result[key].messaged_user_id,
                    data.result[key].message,
                    data.result[key].date,
                    data.result[key].time,
                    data.result[key].status
                    ));
                }

      
                
      
              }
      
            });
          }

          if(data.session_id != ''){

            let body = {
              action : 'updateConvoStatus',
              receiversId : this.receiversId,
              session_id : data.session_id
            };
            this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
              if(data.success){
                
        
              }
        
            });
      
          }


        }

      });
    });


    let body2 = {
      action : 'getuserdata',
      user_id : val
    };
    this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
      if(data.success){
       

        if(data.result.nickname == ""){
          
          if(data.result.login_type_id == "3"){
            this.nickname = data.result.mobile_num;
          }
          else if(data.result.login_type_id == "4"){
            this.nickname = data.result.email;
          }
          else{
            this.nickname = data.result.fname;
          }
        }
        else{
          this.nickname = data.result.nickname;
        }
      }

    });

  }

  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

}
