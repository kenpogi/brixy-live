import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-readytogolive',
  templateUrl: './readytogolive.page.html',
  styleUrls: ['./readytogolive.page.scss'],
})
export class ReadytogolivePage implements OnInit {


  myBroadcast: string; "1";

  constructor(
    private storage: Storage,
    private router: Router) { }

  goBack(){
    this.router.navigate(['termsconditions']);
  }
  goLive(){
    this.storage.set('broadcast_type', this.myBroadcast);
    this.router.navigate(['livestream']);
  }

  ngOnInit() {
    this.myBroadcast = '1';
  }

}
