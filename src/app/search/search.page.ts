import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { Follow } from '../shared/model/follow.model';
import { ModalController } from '@ionic/angular';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  search: string = '';

  userProfiles: Follow[];

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public modalController: ModalController,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  searchProfile(){
    let body = {
      action : 'searchProfile',
      search: this.search
    };
    console.log("searchbody:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'followers.php').subscribe(data => {
     
      if(data.success){
        const followers1: Follow[] = [];

        var picture: string = '';
        console.log("result search Profile:"+JSON.stringify(data));
        for(const key in data.result){


          picture = (data.result[key].profile_photo == '') ? '' :
          "http://"+this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

          followers1.push(new Follow(
            data.result[key].user_id,
            data.result[key].fname,
            data.result[key].lname,
            data.result[key].city,
            data.result[key].country,
            picture,
            data.result[key].user_level,
            data.result[key].followers,
            data.result[key].badge_name
            ));
        }

        this.userProfiles = followers1;
         
      }
    });
  }

  goBack(){
    this.router.navigate(['tabs']);
  }

  async openliveprofile(user_id) {
        const modal = await this.modalController.create({
          component: LiveprofilePage,
          cssClass: 'liveprofilemodalstyle',
          componentProps: { 
            liveStreamProfileId: user_id
          }
        });
        return await modal.present();
      }

}
