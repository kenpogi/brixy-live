import { Component, OnInit } from '@angular/core';
import { SystemNotif } from '../shared/model/systemnotif.model';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-systemnotif',
  templateUrl: './systemnotif.page.html',
  styleUrls: ['./systemnotif.page.scss'],
})
export class SystemnotifPage implements OnInit {


  systemNotif: SystemNotif[] = [];

  constructor(private storage: Storage,
    private postPvdr: PostProvider) { }

  ngOnInit() {
    this.plotData();
  }

  plotData(){


    this.storage.get("brixy_user_id").then((user_id) => {

      let body3 = {
        action : 'getMySystemNotifications',
        user_id : user_id
      };
      this.postPvdr.postData(body3, 'system_notification.php').subscribe(data => {
        
        if(data.success){


          const notificationList: SystemNotif[] = [];
          for(const key in data.result){
            
            notificationList.push(new SystemNotif(
              data.result[key].id,
              data.result[key].notification_message,
              data.result[key].date,
              data.result[key].time
              ));

          }

          this.systemNotif = notificationList;
          
        }
      });
    });


  }

  goBack(){
    window.history.back();
  }

}
