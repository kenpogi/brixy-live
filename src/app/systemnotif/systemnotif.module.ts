import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SystemnotifPageRoutingModule } from './systemnotif-routing.module';

import { SystemnotifPage } from './systemnotif.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SystemnotifPageRoutingModule
  ],
  declarations: [SystemnotifPage]
})
export class SystemnotifPageModule {}
