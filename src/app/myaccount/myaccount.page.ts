import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {
  user: any;
  id: any;
  picture: any;
  fname: any;
  lname: any;
  city: any;
  country: any;
  complete_pic: any;

  user_level: number;
  user_locator_id : string;
  user_experience : number;
  gold_bar : number;
  bracket_to : number;
  user_coins : number;

  totalFollower: number;
  totalFollowing: number;
  badge_name : string;

  vip_package = "";
 
  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private storage: Storage
    ) { }

  goBack(){
    this.router.navigate(['tabs']);
  }
  goFavorites(){
    this.router.navigate(['tabs/tab2']);
  }
  goSettings(){
    this.router.navigate(['settings']);
  }
  goVIP(){
    this.router.navigate(['vipackage']);
  }
  goEditProfile()
  {
    this.router.navigate(['editprofile']);
  }
  goWallet(){
    this.router.navigate(['wallet']);
  }
  goPaymentRequest(){
    this.router.navigate(['payoneerid']);
  }
  goPublished(){
    this.router.navigate(['mybroadcast']);
  }
  goStore(){
    this.router.navigate(['tab4']);
 }
 goVIPPackage(){
  this.router.navigate(['vippackage']);
 }
 goPaymentmethod(){
  this.router.navigate(['paymentsettings']);
 }
 update_photo(){
  this.router.navigate(['uploadphoto']);
}

checkSubscription(){

  this.storage.get("brixy_user_id").then((user_id) => {

    let body2 = {
      action : 'checkSubscription',
      user_id : user_id
    };
    this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
      
      if(data.success){
        for(const key in data.result){
          this.vip_package = data.result[key].vip_package_id;
        }
      }
    });

  });
}

  ngOnInit() {
    //this.loadUser();
  }



  ionViewWillEnter() {
    this.loadUser();
    this.plotData();
    this.checkSubscription();
  }

  plotData(){

    this.storage.get('user_id').then((user_id) => {
      
      let body2 = {
        action : 'getFollow',
        type : 1,
        user_id : user_id
      };
      this.postPvdr.postData(body2, 'followers.php').subscribe(data => {
        
        if(data.success){
          this.totalFollower = data.result;
           
        }
      });


      let body4 = {
        action : 'getFollow',
        type : 2,
        user_id : user_id
      };
      this.postPvdr.postData(body4, 'followers.php').subscribe(data => {
        
        if(data.success){
          this.totalFollowing = data.result;
        }
      });

    });
  }

   loadUser(){

      this.storage.get("brixy_user_id").then((user_id) => {
              let body = {
                action : 'getuserdata',
                user_id : user_id
              };
              this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
                if(data.success){
                  this.id = data.result.id;
                  this.picture = data.result.photo;

                  this.storage.set('id', this.id);
                  this.storage.set('picture', this.picture);
                
                  //this.complete_pic = "http://192.168.1.5/brixy-live/images/"+this.picture;
                  this.complete_pic = this.postPvdr.myServer()+"/brixy-live/images/"+this.picture;
                  console.log("mycompletePic:"+this.complete_pic);
                  this.id = data.result.id;
                  this.fname = data.result.fname;
                  this.lname = data.result.lname;
                  this.city = data.result.city;
                  this.country = data.result.country;
                  this.user_locator_id = data.result.user_locator_id;
                  this.user_level = data.result.user_level;

                  this.user_experience = data.result.user_experience;
                  this.gold_bar = data.result.gold_bar;
                  this.user_coins = data.result.user_coins;

                  this.badge_name = data.result.badge_name;
                  this.bracket_to = data.result.bracket_to;

                  // console.log("user_leveldd:"+this.user_level);
                  // console.log("user_locator_id:"+this.user_locator_id);
                } else {
                  this.id = "0";
                  this.fname = "Anonymous";
                  this.lname = "User";
                  this.city = "Unknown";
                  this.country = "Address";
                  this.user_locator_id = "0";
                  this.user_level = 1;
                }
              });
      });
    
    // this.storage.get('email').then((email) => {
    //   if(email){

    //     this.storage.get('login_used').then((login) => {
    //       if(login){
    //         let login_type = login==="facebook" ? 1 : 2;
    //           let body = {
    //             action : 'getuserdata',
    //             login_used : login_type,
    //             email : email
    //           };
    //           this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
    //             if(data.success){
    //               this.id = data.result.id;
    //               this.picture = data.result.photo;

    //               this.storage.set('id', this.id);
    //               this.storage.set('picture', this.picture);
                
    //               this.complete_pic = "http://192.168.1.2/brixy-live/images/"+this.picture;
    //               //this.complete_pic = "http://165.22.242.129/brixy-live/images/"+this.picture;
    //               this.id = data.result.id;
    //               this.fname = data.result.fname;
    //               this.lname = data.result.lname;
    //               this.city = data.result.city;
    //               this.country = data.result.country;
    //               this.user_locator_id = data.result.user_locator_id;
    //               this.user_level = data.result.user_level;
    //               console.log("user_leveldd:"+this.user_level);
    //               console.log("user_locator_id:"+this.user_locator_id);
    //             } else {
    //               this.id = "0";
    //               this.fname = "Anonymous";
    //               this.lname = "User";
    //               this.city = "Unknown";
    //               this.country = "Address";
    //               this.user_locator_id = "0";
    //               this.user_level = 1;
    //             }
    //           });
    //       } else {
    //         this.id = "00";
    //         this.fname = "Anonymous";
    //         this.lname = "User";
    //         this.city = "Unknown";
    //         this.country = "Address";
    //         this.user_locator_id = "0";
    //         this.user_level = 1;
    //       }
    //     });

    //   } else {
    //     this.storage.get('mobile_num').then((mobile_num) => {
    //       if(mobile_num){
    //         let body = {
    //           action : 'getuserdata_mobile',
    //           mobile_num : mobile_num,
    //         };
    //         this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
    //           if(data.success){
    //             this.id = data.result.id;
    //             this.picture = data.result.photo;

    //             this.storage.set('id', this.id);
    //             this.storage.set('picture', this.picture);

    //             this.complete_pic = "http://192.168.1.2/brixy-live/images/"+this.picture;
    //            // this.complete_pic = "http://165.22.242.129/brixy-live/images/"+this.picture;
    //             this.fname = data.result.fname;
    //             this.lname = data.result.lname;
    //             this.city = data.result.city;
    //             this.country = data.result.country;
    //             this.user_locator_id = data.result.user_locator_id;
    //             this.user_level = data.result.user_level;
    //           } else {
    //             this.id = "0000";
    //             this.fname = "Anonymous";
    //             this.lname = "User";
    //             this.city = "Unknown";
    //             this.country = "Address";
    //             this.user_locator_id = "0";
    //             this.user_level = 1;
    //           }
    //         });

    //       } else {
    //         this.id = "000";
    //         this.fname = "Anonymous";
    //         this.lname = "User";
    //         this.city = "Unknown";
    //         this.country = "Address";
    //         this.user_locator_id = "0";
    //         this.user_level = 1;
    //       }
    //     });
        
    //   }
    // });
    
  }

}
