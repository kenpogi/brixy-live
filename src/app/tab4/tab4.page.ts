import { Component, OnInit, ViewChild } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { Router } from '@angular/router';
import { IonSlides, ModalController } from '@ionic/angular';
import { GiftbronzeComponent } from './../giftbronze/giftbronze.component';
import { GiftgoldComponent } from './../giftgold/giftgold.component';
import { GiftsilverComponent } from './../giftsilver/giftsilver.component';
import { GiftpopularComponent } from './../giftpopular/giftpopular.component';
import { PopoverController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { coinModel } from '../shared/model/coin.model';
import { PurchasecoinsPage } from '../purchasecoins/purchasecoins.page';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;


  bronzeGiftCount = 0;
  silverGiftCount = 0;
  goldGiftCount = 0;
  popularGiftCount = 0;

  gold_bar: number = 0;
  user_coins : number = 0;

  coinsList : coinModel[] = [];

  paymentAmount: string = '10';
  currency: string = 'USD';
  currencyIcon: string = '$';

  constructor(
    private router: Router,
    private payPal: PayPal,
    private popoverCtrl: PopoverController,
    public modalController: ModalController,
    private storage: Storage,
    private postPvdr: PostProvider,
    
    ) { }

    


  payWithPaypal() {
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AV77EKjqxfPSzffflV1bweHwHKDZrYUSGHdX89rh4ZG3H9B0gVixmtDj5pdZ8dBrz-sl-msq2llbULx6'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.paymentAmount, this.currency, 'Description', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          console.log("this is res:"+JSON.stringify(res));
          console.log("successfully paid");
          // Successfully paid
        }, () => {
          // Error or render dialog closed without being successful
          console.log("error in render dialog paid");
        });
      }, () => {
        // Error in configuration
        console.log("error in configuration paid");
      });
    }, () => {
      console.log("error in initialization paid");
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }

  goWallet(){
    this.router.navigate(['wallet']);
  }

  toAccount(){
    this.router.navigate(['myaccount']);
  }
  
  goBack(){
    window.history.back();
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  async bronze(ev: any) {
    const popover = await this.popoverCtrl.create({
        component: GiftbronzeComponent,
        event: ev,
        animated: true,
        showBackdrop: true
    });
    return await popover.present();
}
async silver(ev: any) {
  const popover = await this.popoverCtrl.create({
      component: GiftsilverComponent,
      event: ev,
      animated: true,
      showBackdrop: true
  });
  return await popover.present();
}
async gold(ev: any) {
  const popover = await this.popoverCtrl.create({
      component: GiftgoldComponent,
      event: ev,
      animated: true,
      showBackdrop: true
  });
  return await popover.present();
}
async popular(ev: any) {
  const popover = await this.popoverCtrl.create({
      component: GiftpopularComponent,
      event: ev,
      animated: true,
      showBackdrop: true
  });
  return await popover.present();
}


  ngOnInit() {
    console.log("hello tab4");
    this.plotData();
  }

  async purchaseCoins(coins){
    // console.log("coins:"+coins.id);
    // console.log("coins:"+coins.coins);
    // console.log("coins:"+coins.amount);


    const modal = await this.modalController.create({
      component: PurchasecoinsPage,
      cssClass: 'purchasecoins',
      componentProps: { coin: coins}
    });

      modal.onDidDismiss()
      .then((data) => {
        //const user = data['data']; // Here's your selected user!
        this.showCoinsandGold();
      });

    return await modal.present();

  }

  plotData(){

    
    this.showCoinsandGold();
      
    let body2 = {
      action : 'showCoins'
    };
    this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
      
      if(data.success){
        const coins: coinModel[] = [];
        for(const key in data.result){

          coins.push(new coinModel(
            data.result[key].id,
            data.result[key].coins,
            data.result[key].amount
          ));
        }

        this.coinsList = coins;
      }
    });
  }

  showCoinsandGold(){

    this.storage.get("brixy_user_id").then((user_id) => {

      let body = {
        action : 'getGold_bar',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
        
        if(data.success){
          for(const key in data.result){
            this.gold_bar = data.result[key].gold_bar;
            this.user_coins = data.result[key].user_coins;
            
          }
        }
      });
    });
  }

}
