import { Component, OnInit } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { ModalController, ToastController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { coinModel } from '../shared/model/coin.model';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-purchasecoins',
  templateUrl: './purchasecoins.page.html',
  styleUrls: ['./purchasecoins.page.scss'],
})
export class PurchasecoinsPage implements OnInit {

  coin : coinModel;


  paymentAmount: string = '10';
  currency: string = 'USD';
  currencyIcon: string = '$';

  constructor(
    navParams: NavParams, 
    private storage: Storage, 
    private payPal: PayPal,
    public modalCtrl: ModalController,
    private toastController: ToastController,
    private postPvdr: PostProvider
    ) {

    this.coin = navParams.get('coin');

   }

  ngOnInit() {
  }

  payWithPaypal() {
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AV77EKjqxfPSzffflV1bweHwHKDZrYUSGHdX89rh4ZG3H9B0gVixmtDj5pdZ8dBrz-sl-msq2llbULx6'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.coin.amount+'', this.currency, 'Description', 'sale');
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          console.log("this is res:"+JSON.stringify(res));
          console.log("successfully paid");
          this.purchaseCoins();
          // Successfully paid
        }, () => {
          // Error or render dialog closed without being successful
          console.log("error in render dialog paid");
        });
      }, () => {
        // Error in configuration
        console.log("error in configuration paid");
      });
    }, () => {
      console.log("error in initialization paid");
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }

  closeModal()
    {
      this.modalCtrl.dismiss();
    }

    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Purchased the coins successfully.',
        duration: 3000
      });
      toast.present();
    }

    purchaseCoins(){

      console.log("mipalit lamang pud");
      console.log("coin Id:"+this.coin.id);
      console.log("coin coins:"+this.coin.coins);
      console.log("coin amount:"+this.coin.amount);
      this.modalCtrl.dismiss();

      this.storage.get("brixy_user_id").then((user_id) => {

        let body2 = {
          action : 'purchaseCoins',
          user_id : user_id,
          coin_id : this.coin.id,
          coins : this.coin.coins,
          amount : this.coin.amount
          
        };
        this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
          
          if(data.success){
            this.presentToast();
             
          }
        });

      });



    }

}
