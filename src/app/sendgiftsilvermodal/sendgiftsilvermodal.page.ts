import { Component, OnInit } from '@angular/core';
import { NavParams, ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import {Howl, Howler} from 'howler';
import { PostProvider } from 'src/providers/credential-provider';
import { Storage } from '@ionic/storage';
import { giftModel } from '../shared/model/gifts.model';



@Component({
  selector: 'app-sendgiftsilvermodal',
  templateUrl: './sendgiftsilvermodal.page.html',
  styleUrls: ['./sendgiftsilvermodal.page.scss'],
})
export class SendgiftsilvermodalPage implements OnInit {
  silver : giftModel;
  player: Howl = null;
  live_user_id : string;

  giftQuantity: string = '1';

  user_experience: number = 0;
  user_coins : number = 0;

  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    private storage: Storage,
    private toastController: ToastController,
    private postPvdr: PostProvider) 
    {
    this.silver = navParams.get('silver');
    this.live_user_id = navParams.get('live_user_id');
   }
   closeModal()
   {
     this.modalCtrl.dismiss();
     Howler.volume(0.0);
   }

  ngOnInit() {
    this.showCoinsandGold();
  }

  showCoinsandGold(){

    this.storage.get("brixy_user_id").then((user_id) => {

      let body = {
        action : 'getGold_bar',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
        
        if(data.success){
          for(const key in data.result){
            this.user_experience = data.result[key].user_experience;
            this.user_coins = data.result[key].user_coins;
            
          }
        }
      });
    });
  }

  
  ionViewWillLeave(){
    Howler.volume(0.0);
  }

  sendGift(){
      console.log("olrayt");
    this.storage.get("brixy_user_id").then((user_id) => {

      let body = {
        action : 'checkBalance',
        user_id : user_id
        
      };
      console.log("checkBalance:"+JSON.stringify(body));
      this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
        
        if(data.success){

          if(parseInt(data.user_coins)  >= (parseInt(this.silver.price) * parseInt(this.giftQuantity))){

            let body2 = {
              action : 'sendGifts',
              user_id : user_id,
              giftQuantity: parseInt(this.giftQuantity),
              live_user_id : this.live_user_id,
              economy_id : this.silver.id,
              amount : this.silver.price
              
            };
            console.log("sendGifts:"+JSON.stringify(body2));
            this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
              
              if(data.success){
                this.modalCtrl.dismiss();
                this.presentToast('Sent gift successfully.');
                 this.checkUserLevel(user_id);
              }
            });

          }
          else{
            this.presentToast('Insufficient balance of coins.');
          }
          
        }
        else{
          this.modalCtrl.dismiss();
          this.presentToast('Error.');
        }
      });
      
      
      

    });


  }

  checkUserLevel(user_id){
    let body2 = {
      action : 'checkUserLevel',
      user_id : user_id,
      live_user_id : this.live_user_id,
      
    };
    console.log("sendGifts:"+JSON.stringify(body2));
    this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
      
      if(data.success){
        if(data.upgraded){
          this.presentToast(data.message);
        }
        
      }
    });
    // for leveling up of livestreamer
    let body3 = {
      action : 'checkUserLevel',
      user_id : this.live_user_id
      
    };
    this.postPvdr.postData(body3, 'brixy-store.php').subscribe(data => {
      
      if(data.success){
        if(data.upgraded){
         // this.presentToast(data.message);
         // nothing to do in here
        }
        
      }
    });
  }

  async presentToast(toastMessage) {
    const toast = await this.toastController.create({
      message: toastMessage,
      duration: 3000
    });
    toast.present();
  }

}
