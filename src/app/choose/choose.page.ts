import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {
  user:any = {};
  userData : any;
  loader:any;
  ui = null;
  facebookResponse: FacebookLoginResponse;
  user_status_id : string;


  login_email : string;
  login_pass : string;

  constructor(
    private router: Router,
    public alertCtrl: AlertController,
    private loadingCtrl : LoadingController,
    public toastController: ToastController,
    public afAuth: AngularFireAuth,
    public googlePlus: GooglePlus,
    private fb: Facebook,
    private postPvdr: PostProvider,
    private storage: Storage
    ) { }

    async loading(){
      //show loading
      this.loader = await this.loadingCtrl.create({
        message: '<img src="assets/icon/brixylogo.png" style="height: 25px" height="auto" alt="loading...">',
        translucent: true,
        showBackdrop: false,
        spinner: null,
        duration: 2000
      });
      this.loader.present();
    }

    async presentToastFailed() {
      const toast = await this.toastController.create({
        message: 'Login Failed.',
        duration: 3000
      });
      toast.present();
    }
    prepareToast(){
      if(this.userData.first_name === null || this.userData.first_name === undefined || this.userData.first_name === 'null'){
        //this.presentToastPreGoogle();
        // *
        this.loader.dismiss();
      } else {
        this.storage.set('email', this.userData.email);
        this.storage.set('login_used', "google");
        this.storage.set('fname', this.userData.first_name);

        let body = {
          action : 'getUserId',
          email : this.userData.email,
          login_type: 2
        };

        this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
          if(data.success){
            this.user_status_id = data.user_status_id;
            console.log("picture ng anak:"+data.user_status_id);
            this.storage.set("brixy_user_id", data.user_id);
            console.log("user_id: google used:"+data.user_id);
            this.accountValidator(data.user_status_id);
          }
        });
        this.loader.dismiss();
        
      }
    }
    async presentToast(toastMessage) {
      const toast = await this.toastController.create({
        message: toastMessage,
        duration: 3000
      });
      toast.present();
    }
    // async presentToastPreGoogle() {
    //   const toast = await this.toastController.create({
    //     message: 'You can now login using Google. Please tap again the button.',
    //     duration: 5000
    //   });
    //   toast.present();
    // }
    async presentAlertnoFbEmail() {
      const alert = await this.alertCtrl.create({
      message: 'An error occurred during the sign-up registration of your Facebook account. Please verify your email in Facebook, or login through Google or SMS to register.',
      subHeader: 'Verify email in Facebook.',
      buttons: ['OK']
     });
     await alert.present(); 
  }
    
  google(){
    console.log("hey yousdfdfegfh");
    this.googlePlus.login({}).then(async res => {
      console.log("hey youe:"+res);
      this.loading();
      this.userData = {id : res.userId, email : res.email, first_name : res.givenName, username : res.displayName, last_name : res.familyName};
      this.googleconnect();
    });
  }

  async googleconnect(){

    let body = {
      action : 'checkgoogle',
      id : this.userData.id,
      email : this.userData.email,
      first_name : this.userData.first_name,
      last_name : this.userData.last_name,
      username : this.userData.username,
    };
    console.log("bodybodybody:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {

      // let body2 = {
      //   action : 'getUserId',
      //   email : this.userData.email,
      //   login_type : 2
      // };

      // this.postPvdr.postData(body2, 'credentials-api.php').subscribe(data => {
      //   if(data.success){
      //     this.storage.set("brixy_user_id", data.user_id);
      //   }
      // });
      this.prepareToast();
    });
  }

  facebook(){
    console.log("here in facebook");
    this.fb.login(['public_profile', 'email']).then((res: FacebookLoginResponse) => {
       this.fb.api('me?fields=id,name,email,first_name', []).then(profile => {
         console.log("here again in facebook");
        if(profile['email'] === undefined || profile['email'] === 'undefined'){
          this.presentAlertnoFbEmail();
         // this.presentToastFailed();
          this.fb.logout();
          this.loader.dismiss();
        }else{
          this.loading();
          this.userData = {id : profile['id'], email: profile['email'], first_name: profile['first_name'], username: profile['name']};
          this.storage.set('email', profile['email']);
          this.storage.set('login_used', "facebook");
          this.storage.set('fname', profile['first_name']);
          this.fbconnect();
        }
       });
     }
     ).catch(e => this.presentToastFailed());
  }

  async fbconnect(){

    

    let body = {
      action : "checkfacebook",
      id : this.userData.id,
      email : this.userData.email,
      first_name : this.userData.first_name,
      username : this.userData.username
    };

    
    this.postPvdr.postData(body, 'credentials-api.php').subscribe( data => {

     
      let body2 = {
        action : 'getUserId',
        email : this.userData.id,
        login_type : 1
      };

      this.postPvdr.postData(body2, 'credentials-api.php').subscribe(data => {
        if(data.success){

          this.user_status_id = data.user_status_id;
          this.storage.set("brixy_user_id", data.user_id);
          console.log("user_id: facebook used:"+data.user_id);
          this.accountValidator(data.user_status_id);
        }
      });
      this.loader.dismiss();
      
      
      
    });
  }

  accountValidator(accountId){
    console.log("user_status_id:"+accountId);
    if(accountId == '2'){
      this.presentToast('Your account is Banned!');
    }
    else if(accountId == '3'){
      this.presentToast('Your account is Suspended!');
    }
    else if(accountId == '1'){
      this.presentToast('Welcome to Brixy Live, '+this.userData.first_name+'!');
       this.router.navigate(["/tabs"]);
    }
  }

  forgotpass(){
    this.router.navigate(['forgotpassword']);
  }
  mobile(){
    this.router.navigate(['/mobilee']);
  }

  // emailLogin(){
  //   this.router.navigate(['/email-login']);
  // }
  signUp(){
    console.log("email:"+this.login_email);
   // console.log("password:"+this.password);

    let body = {
      action : 'emailLogin',
      email : this.login_email,
      //password : this.password
    };

    console.log("bodybodykoko:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
      if(data.success){
        this.storage.set("brixy_user_id", data.user_id);
        // console.log("user_id:"+data.user_id);
        // console.log("email_code:"+data.email_code);

        let body2 = {
          action : 'send-email-code',
          sentTo : this.login_email,
          sentToCode : data.email_code
        };

        console.log("bodyisda:"+JSON.stringify(body2));
        this.postPvdr.sendCode(body2).subscribe(data2 => {
         //console.log("status:"+data.email_code);
         this.router.navigate(['/email-code', data.email_code]);
        });

      }
    });


  }
  // toGuest(){
  //   this.router.navigate(['tabs']);
  // }

  ngOnInit() {
    this.afAuth.auth.signOut();
    this.storage.get("brixy_user_id").then((user_id) => {


      console.log("user_id in choose:"+user_id);
      if( user_id != null){
        this.presentToast('Welcome to Brixy Live!');
        this.router.navigate(["/tabs"]);
      }  
      

  });
    //this.storage.clear();
    //console.log(this.afAuth.auth);
  }
   
  
}
