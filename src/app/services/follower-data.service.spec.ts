import { TestBed } from '@angular/core/testing';

import { FollowerDataService } from './follower-data.service';

describe('FollowerDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FollowerDataService = TestBed.get(FollowerDataService);
    expect(service).toBeTruthy();
  });
});
