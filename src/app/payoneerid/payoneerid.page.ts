import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-payoneerid',
  templateUrl: './payoneerid.page.html',
  styleUrls: ['./payoneerid.page.scss'],
})
export class PayoneeridPage implements OnInit {

  payoneerId: string = "";

  constructor(private router: Router,
    private storage: Storage,
    private postPvdr: PostProvider) { }

  ngOnInit() {
    this.plotData();
  }

  goBack(){
    // this.router.navigate(['myaccount']);
    window.history.back();
  }
  goPaymentreq(){
    this.router.navigate(['paymentrequest']);
  }
  addPaymentId(){

    this.storage.get("brixy_user_id").then((user_id) => {

      let body3 = {
        action : 'addPayoneerId',
        user_id : user_id,
        payment_type_id : '1',
        payment_id : this.payoneerId
        
      };
      console.log("get:"+JSON.stringify(body3));
      this.postPvdr.postData(body3, 'payment.php').subscribe(data => {
        
        if(data.success){

          this.router.navigate(['paymentrequest']);
        }
      });
    });
    
    
  }

  plotData(){


    this.storage.get("brixy_user_id").then((user_id) => {

      let body3 = {
        action : 'getPayoneerId',
        user_id : user_id
      };
      
      this.postPvdr.postData(body3, 'payment.php').subscribe(data => {
        
        if(data.success){

          for(const key in data.result){
            
              this.payoneerId = data.result[key].payment_id
            
          }

          
        }
      });
    });


  }

}
