import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterPage } from '../register/register.page';
import { ModalController, IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { UserLive } from './user.model';
import { Announcement } from '../shared/model/announcement.model';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { RankingModel } from '../shared/model/follow.model';


@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  publishers: any = {};

  @ViewChild('slides', { static: true }) slider: IonSlides;

  @ViewChild('slidesRanking', { static: true }) sliderRanking: IonSlides;

  segment = 0;
  segmentRanking = 0;

  withresult : boolean = false;
  withresult21 : boolean = false;

  livestreamers: UserLive[];

  announcement: Announcement[] = [];

  livestreamersPrivate: UserLive[];

  rankingList: RankingModel[]; //streamers;

  rankingListViewers: RankingModel[];

  constructor(
    private router: Router,
    private modalController: ModalController,
    private storage: Storage,
    private postPvdr: PostProvider
    ) {}

    // openlive(){
    //   this.router.navigate(['viewlive']);
    // }

    toAccount(){
      this.router.navigate(['myaccount']);
    }
    goVideo(id){
      console.log("id publishers:"+id);
     // this.followerService.setData(id);
      this.router.navigateByUrl('/viewlive/'+id);
     // this.router.navigate(['viewlive']);
    }
    goHome(){
      this.router.navigate(['tabs']);
    }

    async segmentChanged() {
      await this.slider.slideTo(this.segment);
    }

    async segmentRankingChanged() {
      await this.sliderRanking.slideTo(this.segmentRanking);
    }

    async slideChanged() {
      this.segment = await this.slider.getActiveIndex();
    }

    async slideRankingChanged() {
      this.segmentRanking = await this.sliderRanking.getActiveIndex();
    }

    async openRegistermodal() {
      const modal = await this.modalController.create({
        component: RegisterPage,
        cssClass: 'registermodalstyle'
      });
      return await modal.present();
    }
    
    async check_user_exists(){

      this.storage.get("brixy_user_id").then((user_id) => {

        let body = {
          action : 'check_user_exists',
          user_id : user_id
        };
        
        this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
          if(data.success){
            this.openRegistermodal();
          }
        });
      });
    }


  ngOnInit() {
      //this.fetch_publishers(); //commented to prevent going to publishers in previous database design
    // this.check_user_exists();
    // this.loadUser();
  }

  ionViewWillEnter() {
    this.check_user_exists();
    this.loadUser();
  }

  loadUser(){
        let body = {
          action : 'getUsers_broadcast',
          livestream_type : '1'
        };
        console.log(JSON.stringify(body));
        this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
          
          if(data.success){

            const livestream_users: UserLive[] = [];

            var pictureProfile: string = '';

            for(const key in data.result){
              pictureProfile = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
              livestream_users.push(new UserLive(
                data.result[key].user_id,
                data.result[key].stream_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                pictureProfile,
                data.result[key].user_level,
                data.result[key].badge_name,
                data.result[key].viewers,
                data.result[key].broadcast_topic
                )
                
                );
              console.log(data.result[key].user_id);
              this.withresult = true;
            }

            this.livestreamers = livestream_users;
          }
        });


        let body221 = {
          action : 'getUsers_broadcast',
          livestream_type : '2'
        };
        console.log("nilito:"+JSON.stringify(body221));
        this.postPvdr.postData(body221, 'credentials-api.php').subscribe(async data => {
          
          if(data.success){

            const livestream_users21: UserLive[] = [];

            var pictureProfile21: string = '';

            for(const key in data.result){
              pictureProfile21 = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
              livestream_users21.push(new UserLive(
                data.result[key].user_id,
                data.result[key].stream_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                pictureProfile21,
                data.result[key].user_level,
                data.result[key].badge_name,
                data.result[key].viewers,
                data.result[key].broadcast_topic
                )
                
                );
              console.log(data.result[key].user_id);
              this.withresult21 = true;
            }

            this.livestreamersPrivate = livestream_users21;
          }
        });



          let body2 = {
            action : 'showRanking'
          };
          this.postPvdr.postData(body2, 'followers.php').subscribe(data => {
            //console.log(data);

            var pictureProfile: string = '';
            if(data.success){
  
              const rank_users: RankingModel[] = [];
              for(const key in data.result){

                pictureProfile = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                console.log("myPictures:"+pictureProfile);
                rank_users.push(new RankingModel(
                  data.result[key].user_id,
                  data.result[key].fname,
                  data.result[key].lname,
                  data.result[key].city,
                  data.result[key].country,
                  pictureProfile,
                  data.result[key].user_level,
                  data.result[key].followers,
                  0,
                  data.result[key].badge_name
                  ));
              }
  
               this.rankingList = rank_users;
            }
          });



          let body3 = {
            action : 'showRankingViewers'
          };
          this.postPvdr.postData(body3, 'followers.php').subscribe(data => {
            //console.log(data);

            var picture: string = '';
            if(data.success){
  
              const rank_users: RankingModel[] = [];
              for(const key in data.result){

                picture = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

                rank_users.push(new RankingModel(
                  data.result[key].user_id,
                  data.result[key].fname,
                  data.result[key].lname,
                  data.result[key].city,
                  data.result[key].country,
                  picture,
                  data.result[key].user_level,
                  data.result[key].followers,
                  data.result[key].gift_sum,
                  data.result[key].badge_name
                  ));
              }
  
               this.rankingListViewers = rank_users;
            }
          });


          // for announcement

          let body321 = {
            action : 'showAnnouncement'
          };
          this.postPvdr.postData(body321, 'report.php').subscribe(data => {
            //console.log(data);

            if(data.success){

              const announcementsList: Announcement[] = [];
  
              for(const key in data.result){


                announcementsList.push(new Announcement(
                  data.result[key].id,
                  data.result[key].title,
                  data.result[key].announcment,
                  data.result[key].date,
                  data.result[key].time
                  ));
              }

              this.announcement = announcementsList;
  
            }
          });

        



      }

      async openliveprofile(user_id) {
            const modal = await this.modalController.create({
              component: LiveprofilePage,
              cssClass: 'liveprofilemodalstyle',
              componentProps: { 
                liveStreamProfileId: user_id
              }
            });
            return await modal.present();
          }

      onScroll($event){
        console.log("$event"+$event);
      }
      doRefresh(event) {
        console.log('Begin async operation');
        this.loadUser();
    
        setTimeout(() => {
          console.log('Async operation has ended');
          event.target.complete();
        }, 2000);
      }
   }

