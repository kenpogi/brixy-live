import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';


@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.page.html',
  styleUrls: ['./email-login.page.scss'],
})
export class EmailLoginPage implements OnInit {

  email : string;
  password : string;

  constructor(
    private postPvdr: PostProvider,
    private router: Router,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  signUp(){
    console.log("email:"+this.email);
   // console.log("password:"+this.password);

    let body = {
      action : 'emailLogin',
      email : this.email,
      //password : this.password
    };

    console.log("bodybody:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
      if(data.success){
        this.storage.set("brixy_user_id", data.user_id);
        // console.log("user_id:"+data.user_id);
        // console.log("email_code:"+data.email_code);


        let body2 = {
          action : 'send-email-code',
          sentTo : this.email,
          sentToCode : data.email_code
        };

        console.log("bodyisda:"+JSON.stringify(body2));
        this.postPvdr.sendCode(body2).subscribe(data2 => {
         //console.log("status:"+data.email_code);
         this.router.navigate(['/email-code', data.email_code]);
        });

      }
    });


  }

}
