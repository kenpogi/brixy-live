import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController, ModalController } from '@ionic/angular';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { PostProvider } from '../../providers/credential-provider';
import { Conversations, Users_search } from '../shared/model/chat.model';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  search_input: string;

  searchData: Users_search[] = [];

  myConversations: Conversations[] = [];

  myClass : boolean = false;

  login_user_id : string = '';

  constructor(
    private router: Router,
    public modalController: ModalController,
    private storage: Storage,
    private postPvdr: PostProvider,
    public navCtrl: NavController
    ) { }

  toAccount(){
    this.router.navigate(['myaccount']);
  }
  goMessage(val){
   // this.router.navigateByUrl('/message/'+val);
    this.navCtrl.navigateRoot(['/message/'+val]);
  }

  ngOnInit() {
   // this.plotData();
   // this.initializeData();
  }

  ionViewWillEnter() {
    console.log("ionviewwillenter in tab5 conversation");
    this.plotData();
  }

  ionViewDidEnter()
{
  console.log("ionViewDidEnter entered");
}

  filterData(ev: any){

    

    
    var val = ev.target.value;



    if(val && val.trim().length > 0 ){
      // this.searchData = this.searchData.filter((item) => {
      //   console.log("item:"+JSON.stringify(item));
      //   return (item.nickname.toLowerCase().indexOf(val.toLowerCase()) >-1);
      this.searchData = [];
      this.myClass = true;
      this.initializeData(val);
      // })
    }
    else if(val.length == 0){
      this.searchData = [];
      this.myClass = false;
    }
    else{
      this.searchData = [];
      this.myClass = false;
    }
    console.log("val.trim():"+val.trim());
    console.log("val.length():"+val.length);

  }

  searchedConvo(val){
    //this.router.navigateByUrl('/message/'+val);
    this.navCtrl.navigateRoot(['/message/'+val]);
  }


  searchProfile(){

    this.storage.get("brixy_user_id").then((user_id) => {
      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){


          // for(const key in data.result){
          //   pictureProfile = (data.result[key].profile_photo == '') ? '' :
          //   "http://"+this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
          //   livestream_users.push(new UserLive(
          //     data.result[key].user_id,
          //     data.result[key].stream_id,
          //     data.result[key].fname,
          //     data.result[key].lname,
          //     data.result[key].city,
          //     pictureProfile,
          //     data.result[key].user_level,
          //     data.result[key].badge_name,
          //     data.result[key].viewers,
          //     data.result[key].broadcast_topic
          //     )
              
          //     );
          // }

        } 

      });
    });
  }

  async viewProfile(user_id){

    const modal = await this.modalController.create({
            component: LiveprofilePage,
            cssClass: 'liveprofilemodalstyle',
            componentProps: { 
              liveStreamProfileId: user_id
            }
          });
      
      
      
          return await modal.present();

  }

  plotData(){

    
    this.myConversations = [];

    this.storage.get("brixy_user_id").then((user_id) => {

      this.login_user_id = user_id;

      let body = {
        action : 'getConversation',
        user_id : user_id
      };
       console.log("body of plotdata:"+JSON.stringify(body));
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){

          var myNickname = "";

           for(const key in data.result){

            if(data.result[key].myNickname == ""){
              if(data.result[key].login_type_id == "3"){
                myNickname = data.result[key].mobile_num
              }
              else if(data.result[key].login_type_id == "4"){
                myNickname = data.result[key].email
              }
            }
            else{
              myNickname = data.result[key].myNickname;
            }

            this.myConversations.push(new Conversations(
              data.result[key].user_id,
              myNickname,
              (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo,
              data.result[key].message,
              data.result[key].date_sent,
              data.result[key].time_sent,
              data.result[key].mystatus,
              data.result[key].user_sent
              )
              
            );
          }


        } 

      });
    });

  }

  initializeData(val){

    this.storage.get("brixy_user_id").then((user_id) => {

      let body = {
        action : 'getUsersToChat',
        nickname : val,
        user_id: user_id
      };
     
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){

          var myNickname = "";

           for(const key in data.result){


            if(data.result[key].myNickname == ""){
              if(data.result[key].login_type_id == "3"){
                myNickname = data.result[key].mobile_num
              }
              else if(data.result[key].login_type_id == "4"){
                myNickname = data.result[key].email
              }
            }
            else{
              myNickname = data.result[key].myNickname;
            }

            this.searchData.push(new Users_search(
              data.result[key].user_id,
              myNickname,
              (data.result[key].profile_photo == '') ? '' :
           
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo
              )
              
            );
          }

        } 

      });

    });

    // this.searchData = [
    //   {
    //     "name": "Argie",
    //     "code": "65"
    //   },
    //   {
    //     "name": "Argie1",
    //     "code": "234"
    //   },
    //   {
    //     "name": "Argie2",
    //     "code": "23"
    //   },
    //   {
    //     "name": "Diane",
    //     "code": "345"
    //   }
    //   ,
    //   {
    //     "name": "Diane2",
    //     "code": "4534"
    //   },
    //   {
    //     "name": "Diane3",
    //     "code": "fhf"
    //   },
    //   {
    //     "name": "Lyanna",
    //     "code": "as"
    //   }
    //   ,
    //   {
    //     "name": "Lyanna1",
    //     "code": "fdgfd"
    //   },
    //   {
    //     "name": "Lyanna2",
    //     "code": "mycsdfdsode"
    //   },
    //   {
    //     "name": "Lyanna3",
    //     "code": "sdfg"
    //   }

    // ]
  }

}
