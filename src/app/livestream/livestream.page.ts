import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform,  ModalController  } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Storage } from '@ionic/storage';
import { Socket } from 'ngx-socket-io';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { PostProvider } from '../../providers/credential-provider';

declare var WebRTCAdaptor: any;

const APPLICATION_ID:string = 'bqLflYU6Hz9155PQHIgGEw';


@Component({
  selector: 'app-livestream',
  templateUrl: './livestream.page.html',
  styleUrls: ['./livestream.page.scss'],
})
export class LivestreamPage implements AfterViewInit {
  email:any;
  mobile_num:any;


  login_user_id : string;
  login_nickname: string;
  login_profile_photo : string;

  isBroadcasting = false;
  isPending = false;
  broadcaster: any;
  errListenerId = false;
  broadcastListenerId = false;

  message = '';

  messages = [];

  viewersPic = [];

  webRTCAdaptor: any = null;
   //webRTCAdaptor = null;

  user_id: number;
  fname: string;
  lname: string;
  city: string;
  user_photo: string;
  user_level: number;
  followers: number;
  following: number;

  //webRTCAdaptor: any;
  streamId: string = '';

  constructor(
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private postPvdr: PostProvider,
    public storage: Storage,
    private socket: Socket,
    private toastCtrl: ToastController,
    private router: Router,
    public modalController: ModalController
  ) { 
    //console.log("PLATFORM: "+this.platform.platforms());
    
      if (this.platform.is('cordova')) {
        this.platform.ready().then(() => {
            
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
            result => console.log('Has permission? ', result.hasPermission),
            err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS])
          );
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(
            result => console.log('Has audio permission? ', result.hasPermission),
            err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS])
          );
          
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS]);
        });
      } else {
        alert("Cannot play Live streaming at the moment. Please contact Brixy Live at brixylive.com.");
      }

      platform.ready().then(() => {
        // Using array syntax workaround, since types are not declared.
        if (window['bambuser']) {
          this.broadcaster = window['bambuser']['broadcaster'];
          this.broadcaster.setApplicationId(APPLICATION_ID);
        } else {
          // Cordova plugin not installed or running in a web browser
        }
      });
  }


  // code for bambuser

  async ionViewDidEnter() {
    if (APPLICATION_ID.endsWith('NGEME')) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('Warning: APPLICATION_ID is not set. Get your application id at https://dashboard.bambuser.com/developer and update pages/broadcaster/broadcaster.ts, then rebuild the app.');
    }

    // Engage our Ionic CSS background overrides that ensure viewfinder is visible.
    document.getElementsByTagName('body')[0].classList.add("show-viewfinder");

    if (!this.platform.is('cordova')) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('This Ionic app is currently not running within a Cordova project. Broadcasting is only supported on iOS and Android devices.');
      return;
    }

    await this.platform.ready();

    if (!this.broadcaster) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('Broadcasting plugin not detected. Try running `cordova plugin add cordova-plugin-bambuser` and rebuild your app.');
      return;
    }

    console.log('Starting viewfinder');
    this.broadcaster.showViewfinderBehindWebView();
   

    this.plotDetails();
    this.start2();
    this.start();
  }

  ionViewWillLeave() {
    // Disengage our Ionic CSS background overrides, to ensure the rest of the app looks ok.
    document.getElementsByTagName('body')[0].classList.remove("show-viewfinder");

    console.log('Removing viewfinder');
    if (this.broadcaster) {
      this.broadcaster.hideViewfinder();
    }
  }

  async start() {
    if (this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = await this.toastCtrl.create({
      message: 'Starting broadcast...',
      position: 'middle',
    });
    await toast.present();

    console.log('Starting broadcast');
    try {
      await this.broadcaster.startBroadcast();
      toast.dismiss();
      this.isBroadcasting = true;
      this.isPending = false;

      this.listenForError();
      this.listenForBroadcastId();

    } catch (e) {
      toast.dismiss();
      this.isPending = false;
      alert('Failed to start broadcast');
      console.log(e);
    }
  }

  async stop() {
    if (!this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = await this.toastCtrl.create({
      message: 'Ending broadcast...',
      position: 'middle'
    });
    await toast.present();

    console.log('Ending broadcast');
    try {
      await this.broadcaster.stopBroadcast();
      toast.dismiss();
      this.isBroadcasting = false;
      this.isPending = false;
    } catch (e) {
      toast.dismiss();
      this.isPending = false;
      alert('Failed to stop broadcast');
      console.log(e);
    }
  }

  listenForError() {
    if (this.errListenerId) return;
    this.errListenerId = this.broadcaster.addEventListener('connectionError', async status => {
      this.isBroadcasting = false;
      this.isPending = false;
      const toast = await this.toastCtrl.create({
        message: 'Connection error',
        position: 'middle',
        duration: 3000,
      });
      toast.present();
    });
  }

  listenForBroadcastId() {
    if (this.broadcastListenerId) return;
    this.broadcastListenerId = this.broadcaster.addEventListener('broadcastIdAvailable', async broadcastId => {
      const toast = await this.toastCtrl.create({
        message: `Broadcast id received: ${broadcastId}`,
        position: 'middle',
        duration: 3000,
      });
      toast.present();
    });
  }

  switchCamera() {
    if (this.broadcaster) {
      this.broadcaster.switchCamera();
    }
  }

  // end code for bambuser

  ngAfterViewInit() {
    // this.start();
    // this.plotDetails();
  }

  plotDetails(){

        this.storage.get("brixy_user_id").then((user_id) =>{


          let body = {
            action : 'getUser_liveData',
            user_id : user_id
          };
          this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
            
            if(data.success){


              for(const key in data.result){

                  this.user_id = data.result[key].user_id;
                  this.fname = data.result[key].fname;
                  this.lname = data.result[key].lname;
                  this.city = data.result[key].city;
                  this.user_photo = (data.result[key].profile_photo == '') ? '' :
                 this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                  this.user_level = data.result[key].user_level;
                  this.followers = data.result[key].followers;
                  this.following = data.result[key].following;
              
              }

              

            }
          });



         


        });// end of user_id
    

    
  }

  goTab5(){
    this.router.navigate(['tabs/tab5']);
  }

  // ionViewWillLeave() {

  //   this.goHome();
  // }

  goHome(){

this.stop();
    this.storage.get("brixy_user_id").then((user_id) =>{

      let body2 = {
        action : 'updateLive',
        user_id : user_id
  
      };
      this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
        
        if(data.success){
          //this.webRTCAdaptor.stop(this.streamId);
          this.router.navigate(['tabs']);
         // this.webRTCAdaptor.stop(this.streamId);

        }

    });

  });
}
  

  openliveprofile() {

    this.storage.get("brixy_user_id").then(async (user_id) => {

      const modal = await this.modalController.create({
              component: LiveprofilePage,
              cssClass: 'liveprofilemodalstyle',
              componentProps: { 
                liveStreamProfileId: user_id,
              }
            });
            return await modal.present();

    });

        
  }

  startPublishing(streamK){
    this.webRTCAdaptor.publish(streamK);
  }

  start2(){

    this.storage.get("broadcast_topic").then((broadcast_topic) => {

      console.log("broadcast:"+broadcast_topic);

      this.storage.get("broadcast_type").then((broadcast_type) => {

        this.storage.get("brixy_user_id").then((user_id) => {
          //var regExpr = /[^a-zA-Z0-9 ]/g;
          // var streamName = user_id;

          this.login_user_id = user_id;

            let dateTime = new Date();
            let body = {
              action : "publisher",
              user_id : user_id,
              livestream_type : broadcast_type,
              date_live : dateTime,
              time_started : dateTime,
              broadcast_topic: broadcast_topic
            };
            console.log("publisher:"+JSON.stringify(body));
            this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
              if(data.success){



                // code for chat

                let body2 = {
                  action : 'getuserdata',
                  user_id : this.login_user_id
                };
                this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
                  if(data.success){
                   
            
                    if(data.result.nickname == ""){
                      
                      if(data.result.login_type_id == "3"){
                        this.login_nickname = data.result.mobile_num;
                      }
                      else if(data.result.login_type_id == "4"){
                        this.login_nickname = data.result.email;
                      }
                      else{
                        this.login_nickname = data.result.fname;
                      }
                    }
                    else{
                      this.login_nickname = data.result.nickname;
                    }
                    this.login_profile_photo = data.result.photo;
          
          
          
                    this.socket.connect();
                
                
                    // Join chatroom
                    console.log("user_id:"+this.user_id+"room:"+this.streamId);
                    this.socket.emit('joinRoom', { 
                      user_id: this.login_nickname,
                      room: this.streamId,
                      profile_picture: this.login_profile_photo
                    });
          
                    this.socket.fromEvent('message').subscribe(message => {
                      this.messages.push(message);
                      console.log("human og send"+ JSON.stringify(message));
                    });
                
                    this.socket.fromEvent('joinRoomMessage').subscribe(message => {
                      console.log("message from server:"+JSON.stringify(message));
                      this.messages.push(message);
                      this.addPictureViewer(message['profile_picture']);
                    });
          
                    this.socket.fromEvent('leaveRoom').subscribe(message => {
                      console.log("message from server leave:"+JSON.stringify(message));
                      this.messages.push(message);
                    });
          
          
                  }// end of success
            
                });





              }
            });
          });// end of storage user_id
      
        }); // end of storage broadcast_type

     }); // end of storage broadcast_topic

  }


  // start1 an dati nga function before bambuser: nagamit pa hin ant media
  start1() {
    
    var pc_config = {
      'iceServers' : [ {
        'urls' : 'stun:stun.l.google.com:19302'
      } ]
    };
  
    var sdpConstraints = {
      OfferToReceiveAudio : false,
      OfferToReceiveVideo : false
        
    };
    var mediaConstraints = {
        video: true,
        audio: true
    };
          
   // var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/")+1);

    var websocketURL = "ws://livebrixylive.com:5080/WebRTCAppEE/websocket";

    var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/")+1);
		var path =  location.hostname + ":" + location.port + appName + "websocket";
    var websocketURL123 =  "ws://" + path;
    console.log("location:"+location.protocol);
    console.log("path:"+path);

		if (location.protocol.startsWith("https")) {
			websocketURL123 = "wss://" + path;
		}

    this.storage.get("broadcast_topic").then((broadcast_topic) => {

      console.log("broadcast:"+broadcast_topic);

      this.storage.get("broadcast_type").then((broadcast_type) => {

        this.storage.get("brixy_user_id").then((user_id) => {
          //var regExpr = /[^a-zA-Z0-9 ]/g;
          // var streamName = user_id;

          this.login_user_id = user_id;

            let dateTime = new Date();
            let body = {
              action : "publisher",
              user_id : user_id,
              livestream_type : broadcast_type,
              date_live : dateTime,
              time_started : dateTime,
              broadcast_topic: broadcast_topic
            };
            console.log("publisher:"+JSON.stringify(body));
            this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
              if(data.success){
                console.log("stream_id:"+data.result);
                console.log("this.user_id:"+user_id);


                
                this.streamId = data.result;
                
                 var webRTCAdaptor = new WebRTCAdaptor({
                  websocket_url: websocketURL,
                  mediaConstraints: mediaConstraints,
                  peerconnection_config: pc_config,
                  sdp_constraints: sdpConstraints,
                  localVideoId: "localVideo",
                // isPlayMode: true,
              
                  callback: function(info) {
                    if (info == "initialized") {
                      console.log("initialized1:"+data.result);
                      //webRTCAdaptor.publish(streamName);
                      console.log("after initialized");
                      webRTCAdaptor.publish(data.result);
                      //this.startPublishing(data.result);
                      console.log("after published");
                    }
                    console.log("info content:"+info);
                  },
                  callbackError: function(error) {
                    //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
                    console.log("error callback: " + error);
                    alert("There was a problem occured while initializing the stream. The required version of your device may not be compatible for streaming or please allow the permissions (Camera and Audio).");
                  }
                });



                // code for chat

                let body2 = {
                  action : 'getuserdata',
                  user_id : this.login_user_id
                };
                this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
                  if(data.success){
                   
            
                    if(data.result.nickname == ""){
                      
                      if(data.result.login_type_id == "3"){
                        this.login_nickname = data.result.mobile_num;
                      }
                      else if(data.result.login_type_id == "4"){
                        this.login_nickname = data.result.email;
                      }
                      else{
                        this.login_nickname = data.result.fname;
                      }
                    }
                    else{
                      this.login_nickname = data.result.nickname;
                    }
                    this.login_profile_photo = data.result.photo;
          
          
          
                    this.socket.connect();
                
                
                    // Join chatroom
                    console.log("user_id:"+this.user_id+"room:"+this.streamId);
                    this.socket.emit('joinRoom', { 
                      user_id: this.login_nickname,
                      room: this.streamId,
                      profile_picture: this.login_profile_photo
                    });
          
                    this.socket.fromEvent('message').subscribe(message => {
                      this.messages.push(message);
                      console.log("human og send"+ JSON.stringify(message));
                    });
                
                    this.socket.fromEvent('joinRoomMessage').subscribe(message => {
                      console.log("message from server:"+JSON.stringify(message));
                      this.messages.push(message);
                      this.addPictureViewer(message['profile_picture']);
                    });
          
                    this.socket.fromEvent('leaveRoom').subscribe(message => {
                      console.log("message from server leave:"+JSON.stringify(message));
                      this.messages.push(message);
                    });
          
          
                  }// end of success
            
                });





              }
            });
          });// end of storage user_id
      
        }); // end of storage broadcast_type

     }); // end of storage broadcast_topic

  }


  addPictureViewer(joinPic){
    console.log("picture of those who join:"+joinPic);
    var viewPic = joinPic == '' ? "assets/icon/brixylogo.png":
    this.postPvdr.myServer()+"/brixy-live/images/"+joinPic;

    let body = {
      viewerPic : viewPic
    }
    this.viewersPic.push(body);

    console.log("vi:"+JSON.stringify(this.viewersPic));
    

  }

  sendMessage() {
    this.socket.emit('send-message', { user_id: this.login_nickname+" : ", room: this.streamId, text: this.message });

 

    let body = {
      action : 'insertConvoInGC',
      user_id : this.login_user_id,
      stream_id : this.streamId,
      message : this.message
    };
    this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
      if(data.success){
        

      }

    });



    this.message = '';

  }




}
