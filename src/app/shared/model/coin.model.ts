export class coinModel {
    constructor(
      public id: number,
      public coins: number,
      public amount: number
    ) {}
  }

export class coinTransactionModel {
  constructor(
    public image: string,
    public dateandtime: string,
    public text: string
  ) {}
}