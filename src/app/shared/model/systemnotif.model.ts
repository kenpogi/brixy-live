export class SystemNotif {
    constructor(
      public id: number,
      public notification: string,
      public date: string,
      public time: string
    ) {}
  }