export class chat {
    constructor(
      public id: string,
      public session_id: string,
      public messaged_user_id: string,
      public message: string,
      public date: string,
      public time: string,
      public status: string
    ) {}
  }

  export class Conversations {
    constructor(
      public id: string,
      public nickname: string,
      public profile_photo: string,
      public message: string,
      public date: string,
      public time: string,
      public status: string,
      public user_sent: string
    ) {}
  }

  export class Users_search {
    constructor(
      public id: string,
      public nickname: string,
      public profile_photo: string
    ) {}
  }
  